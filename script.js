// js-HW6 - user-age-and-password

// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

//   Екранування – це заміна в тексті спеціальних символів на відповідні текстові підстановки тоді, 
//   коли необхідно використовувати такий символ як «звичайний символ мови».
//   Екрануючий символ (\) повідомляє інтерпретатору, що символ, 
//   що слідує за ним, повинен сприйматися як звичайний символ.


// 2. Які засоби оголошення функцій ви знаєте?

//    function declaration, function expression, стрелочные функции. 




// 3. Що таке hoisting, як він працює для змінних та функцій?

//   Hoisting або підняття  –  це можливість отримувати доступ до функцій та змінних до того, 
//   як вони були створені. Це механізм відноситься лише до оголошення функцій та змінних.



// ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser())
//      і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`)
//      і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача
//      у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження.
//      (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`,
//   а також функцій `getAge()` та `getPassword()` створеного об'єкта.



function createNewUser() {
  let newUser = new Object();
  newUser.firstName = prompt("Enter you first name");
  newUser.lastName = prompt("Enter you last name");
  newUser.birthDay = prompt("Enter your date of birth, `dd.mm.yyyy`");
  newUser.getLogin = function () {
  return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
  };
  newUser.getAge = function () {
    return `${new Date().getFullYear() - newUser.birthDay.slice(6)}`;
  };
  newUser.getPassword = function () {
    return `${newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthDay.slice(6)}`;
  };
  return newUser;

}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

